# Docker + Winter CMS

[![Codefresh build status]( https://g.codefresh.io/api/badges/pipeline/webmago/WinterCMS%2Fnew_tag_version?type=cf-1&key=eyJhbGciOiJIUzI1NiJ9.NWJlOGUzNDcwMTVhYjMyNzZhYWRkZDM2.pQiH7foO7vMm38dpreAKoraX03JT3rnF3swoIKkKl78)]( https://g.codefresh.io/pipelines/edit/new/builds?id=608974679fc23d1b5e23f3a4&pipeline=new_tag_version&projects=WinterCMS&projectId=608972f29fc23d085823f399) [![winter CMS Build 1.1.3](https://img.shields.io/github/v/tag/wintercms/winter?label=Winter%20CMS%20version&style=plastic)](https://github.com/wintercms/winter) ![Docker Pulls](https://img.shields.io/docker/pulls/webmago/winter?style=plastic)

The docker images defined in this repository serve as a starting point for [Winter CMS](https://wintercms.com) projects.

Based on [official docker PHP images](https://hub.docker.com/_/php), images include dependencies required by winter, Composer and install the [latest release](https://wintercms.com/changelog).

This is a `docker-octobercm` repository fork from [@Aspendigital](https://github.com/aspendigital/docker-octobercms/)

- [Supported Tags](https://bitbucket.org/webmago/docker-wintercms/src/master/#supported-tags)
- [Quick Start](https://bitbcket.com/webmago/docker-wintercms/src/master/#quick-start)
- [Working with Local Files](https://bitbcket.com/webmago/docker-wintercms/src/master/#working-with-local-files)
- [Database Support](https://bitbcket.com/webmago/docker-wintercms/src/master/#database-support)
- [Cron](https://bitbcket.com/webmago/docker-wintercms/src/master/#cron)
- [Command Line Tasks](https://bitbcket.com/webmago/docker-wintercms/src/master/#command-line-tasks)
- [App Environment](https://bitbcket.com/webmago/docker-wintercms/src/master/#app-environment)

---

## [Supported Tags](#supported-tags)

- `latest`
- `stable-v1.1.3-php7.4-apache`, `stable-v1.1.2-php7.4-apache`, `stable-v1.1.0-php7.4-apache`
- `stable-v1.1.3-php7.4-fpm`, `stable-v1.1.2-php7.4-fpm`, `stable-v1.1.0-php7.4-fpm`
- `stable-v1.1.3-php7.3-apache`, `stable-v1.1.2-php7.3-apache`, `stable-v1.1.0-php7.3-apache`
- `stable-v1.1.3-php7.3-fpm`, `stable-v1.1.2-php7.3-fpm`, `stable-v1.1.0-php7.3-fpm`
- `stable-v1.1.3-php7.2-apache`, `stable-v1.1.2-php7.2-apache`, `stable-v1.1.0-php7.2-apache`
- `stable-v1.1.3-php7.2-fpm`, `stable-v1.1.2-php7.2-fpm`, `stable-v1.1.0-php7.2-fpm`


### Legacy Tags

> Winter CMS v1.1.3+ requires PHP version 7.2 or higher, lower versions are not supported.


## [Quick Start](#quick-start)

To run winter CMS using Docker, start a container using the latest image, mapping your local port 80 to the container's port 80:

```shell
$ docker run -p 80:80 --name winter webmago/winter:latest
# `CTRL-C` to stop
$ docker rm winter  # Destroys the container
```

> If there is a port conflict, you will receive an error message from the Docker daemon. Try mapping to an open local port (-p 8080:80) or shut down the container or server that is on the desired port.

 - Visit [http://localhost](http://localhost) using your browser.
 - Login to the [backend](http://localhost/backend) with the username `admin` and password `admin`.
 - Hit `CTRL-C` to stop the container. Running a container in the foreground will send log outputs to your terminal.

Run the container in the background by passing the `-d` option:

```shell
$ docker run -p 80:80 --name winter -d webmago/winter:latest
$ docker stop winter  # Stops the container. To restart `docker start winter`
$ docker rm winter  # Destroys the container
```

## [Working with Local Files](#working-with-local-files)

Using Docker volumes, you can mount local files inside a container.

The container uses the working directory `/var/www/html` for the web server document root. This is where the winter CMS codebase resides in the container. You can replace files and folders, or introduce new ones with bind-mounted volumes:

```shell
# Developing a plugin
$ git clone git@github.com:aspendigital/oc-resizer-plugin.git
$ cd oc-resizer-plugin
$ docker run -p 80:80 --rm \
  -v $(pwd):/var/www/html/plugins/aspendigital/resizer \
  webmago/winter:latest
```

Save yourself some keyboards strokes, utilize [docker-compose](https://docs.docker.com/compose/overview/) by introducing a `docker-compose.yml` file to your project folder:

```yml
# docker-compose.yml
version: '2.2'
services:
  web:
    image: webmago/winter
    ports:
      - 80:80
    volumes:
      - $PWD:/var/www/html/plugins/aspendigital/resizer
```
With the above example saved in working directory, run:

```shell
$ docker-compose up -d # start services defined in `docker-compose.yml` in the background
$ docker-compose down # stop and destroy
```


## [Database Support](#database-support)

#### SQLite

On build, an SQLite database is [created and initialized](https://bitbcket.com/webmago/docker-wintercms/blob/d3b288b9fe0606e32ac3d6466affd2996394bdca/Dockerfile.template#L54-L57) for the Docker image. With that database, users have immediate access to the backend for testing and developing themes and plugins. However, changes made to the built-in database will be lost once the container is stopped and removed.

When projects require a persistent SQLite database, copy or create a new database to the host which can be used as a bind mount:

```shell
# Create and provision a new SQLite database:
$ touch storage/database.sqlite
$ docker run --rm \
  -v $(pwd)/storage/database.sqlite:/var/www/html/storage/database.sqlite \
  webmago/winter php artisan winter:up

# Now run with the volume mounted to your host
$ docker run -p 80:80 --name winter \
 -v $(pwd)/storage/database.sqlite:/var/www/html/storage/database.sqlite \
 webmago/winter
```

#### MySQL / Postgres

Alternatively, you can host the database using another container:

```yml
#docker-compose.yml
version: '2.2'
services:
  web:
    image: webmago/winter:latest
    ports:
      - 80:80
    environment:
      - DB_TYPE=mysql
      - DB_HOST=mysql #DB_HOST should match the service name of the database container
      - DB_DATABASE=wintercms
      - DB_USERNAME=root
      - DB_PASSWORD=root

  mysql:
    image: mysql:5.7
    ports:
      - 3306:3306
    environment:
      - MYSQL_ROOT_PASSWORD=root
      - MYSQL_DATABASE=wintercms
```
Provision a new database with `winter:up`:

```ssh
$ docker-compose up -d
$ docker-compose exec web php artisan winter:up
```

## [Cron](#cron)

You can start a cron process by setting the environment variable `ENABLE_CRON` to `true`:

```shell
$ docker run -p 80:80 -e ENABLE_CRON=true webmago/winter:latest
```

Separate the cron process into it's own container:

```yml
#docker-compose.yml
version: '2.2'
services:
  web:
    image: webmago/winter:latest
    init: true
    restart: always
    ports:
      - 80:80
    environment:
      - TZ=America/Denver
    volumes:
      - ./.env:/var/www/html/.env
      - ./plugins:/var/www/html/plugins
      - ./storage/app:/var/www/html/storage/app
      - ./storage/logs:/var/www/html/storage/logs
      - ./storage/database.sqlite:/var/www/html/storage/database.sqlite
      - ./themes:/var/www/html/themes

  cron:
    image: webmago/winter:latest
    init: true
    restart: always
    command: [cron, -f]
    environment:
      - TZ=America/Denver
    volumes_from:
      - web
```

## [Command Line Tasks](#command-line-tasks)

Run the container in the background and launch an interactive shell (bash) for the container:


```shell
$ docker run -p 80:80 --name containername -d webmago/winter:latest
$ docker exec -it containername bash
```

Commands can also be run directly, without opening a shell:

```shell
# artisan
$ docker exec containername php artisan env

# composer
$ docker exec containername composer info
```

A few helper scripts have been added to the image:

```shell
# `winter` invokes `php artisan winter:"$@"`
$ docker exec containername winter up

# `artisan` invokes `php artisan "$@"`
$ docker exec containername artisan plugin:install aspendigital.resizer

# `tinker` invokes `php artisan tinker`. Requires `-it` for an interactive shell
$ docker exec -it containername tinker
```


## [App Environment](#app-environment)

By default, `APP_ENV` is set to `docker`.

On image build, a default `.env` is [created](https://bitbucket.org/webmago/docker-wintercms/src/6e9c61b91567f5559d4106558d766e14a1e440f3/Dockerfile.template#lines-52) and [config files](https://bitbucket.org/webmago/docker-wintercms/src/master/config/docker/) for the `docker` app environment are copied to `/var/www/html/config/docker`. Environment variables can be used to override the included default settings via [`docker run`](https://docs.docker.com/engine/reference/run/#env-environment-variables) or [`docker-compose`](https://docs.docker.com/compose/environment-variables/).

> __Note__: Winter CMS settings stored in a site's database override the config. Active theme, mail configuration, and other settings which are saved in the database will ultimately override configuration values.

#### PHP configuration

Recommended [settings for opcache and PHP are applied on image build](https://bitbucket.org/webmago/docker-wintercms/src/6e9c61b91567f5559d4106558d766e14a1e440f3/Dockerfile.template##lines-10:26).

Values set in `docker-oc-php.ini` can be overridden by passing one of the supported PHP environment variables defined below.

To customize the PHP configuration further, add or replace `.ini` files found in `/usr/local/etc/php/conf.d/`.

### Environment Variables


Environment variables can be passed to both docker-compose and winter CMS.

 > Database credentials and other sensitive information should not be committed to the repository. Those required settings should be outlined in __.env.example__

 > Passing environment variables via Docker can be problematic in production. A `phpinfo()` call may leak secrets by outputting environment variables.  Consider mounting a `.env` volume or copying it to the container directly.


#### Docker Entrypoint

The following variables trigger actions run by the [entrypoint script](https://bitbcket.com/webmago/docker-wintercms/blob/master/docker-wn-entrypoint) at runtime.

| Variable | Default | Action |
| -------- | ------- | ------ |
| ENABLE_CRON | false | `true` starts a cron process within the container |
| FWD_REMOTE_IP | false | `true` enables remote IP forwarding from proxy (Apache) |
| GIT_CHECKOUT |  | Checkout branch, tag, commit within the container. Runs `git checkout $GIT_CHECKOUT` |
| GIT_MERGE_PR |  | Pass GitHub pull request number to merge PR within the container for testing |
| INIT_WINTER | false | `true` runs winter up on container start |
| INIT_PLUGINS | false | `true` runs composer install in plugins folders where no 'vendor' folder exists. `force` runs composer install regardless. Helpful when using git submodules for plugins. |
| PHP_DISPLAY_ERRORS | off | Override value for `display_errors` in docker-oc-php.ini |
| PHP_MEMORY_LIMIT | 128M | Override value for `memory_limit` in docker-oc-php.ini |
| PHP_POST_MAX_SIZE | 32M | Override value for `post_max_size` in docker-oc-php.ini |
| PHP_UPLOAD_MAX_FILESIZE | 32M | Override value for `upload_max_filesize` in docker-oc-php.ini |
| UNIT_TEST |  | `true` runs all winter CMS unit tests. Pass test filename to run a specific test. |
| VERSION_INFO | false | `true` outputs container current commit, php version, and dependency info on start |
| XDEBUG_ENABLE | false | `true` enables the Xdebug PHP extension |
| XDEBUG_REMOTE_HOST | host.docker.internal | Override value for `xdebug.remote_host` in docker-xdebug-php.ini |

#### winter CMS app environment config

List of variables used in `config/docker`

| Variable | Default |
| -------- | ------- |
| APP_DEBUG | false |
| APP_KEY | 0123456789ABCDEFGHIJKLMNOPQRSTUV |
| APP_URL | http://localhost |
| APP_LOCALE | en |
| CACHE_STORE | file |
| CMS_ACTIVE_THEME | demo |
| CMS_BACKEND_FORCE_SECURE | false |
| CMS_BACKEND_SKIN | Backend\Skins\Standard |
| CMS_BACKEND_URI | backend |
| CMS_DATABASE_TEMPLATES | false |
| CMS_DISABLE_CORE_UPDATES | true |
| CMS_EDGE_UPDATES | false  (true in `edge` images) |
| CMS_LINK_POLICY | detect |
| DB_DATABASE | - |
| DB_HOST | mysql* |
| DB_PASSWORD | - |
| DB_PORT | - |
| DB_REDIS_HOST | redis* |
| DB_REDIS_PASSWORD | null |
| DB_REDIS_PORT | 6379 |
| DB_SQLITE_PATH | storage/database.sqlite |
| DB_TYPE | sqlite |
| DB_USERNAME | - |
| MAIL_DRIVER | log |
| MAIL_FROM_ADDRESS | no-reply@domain.tld |
| MAIL_FROM_NAME | winter CMS |
| MAIL_SMTP_ENCRYPTION | tls |
| MAIL_SMTP_HOST | - |
| MAIL_SMTP_PASSWORD | - |
| MAIL_SMTP_PORT | 587 |
| MAIL_SMTP_USERNAME | - |
| QUEUE_DRIVER | sync |
| SESSION_DRIVER | file |
| TZ\** | UTC |

* When using a container to serve a database, set the host value to the service name defined in your docker-compose.yml

** Timezone applies to both container and winter CMS  config
---