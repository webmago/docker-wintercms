#!/bin/bash
set -e

# First argument be the version obtained from github repo
# and passed from CF variable
#VTAG=$(echo ${1} | awk '/^(v)((\*)|([0-9]+(\.((\*)|([0-9]+(\.((\*)|([0-9]+)))?)))?))$/')
#VTAG=${VTAG:?"you must define the Version Tag like vX.Y.Z"}

## store the current path, for Codefresh running process
cwd=$(pwd)

# Checkout the Tag
function checkout_tag(){
    bversion="release-${VTAG:1}"
    echo "Branch to create $bversion"

    cd ../winter/
    git fetch --all --tags --prune

    if git show-ref --quiet refs/heads/$bversion; then
      echo "branch ${bversion} already exists!"
    else
       git checkout tags/$VTAG -b $bversion
       echo "branch ${bversion} created"
    fi
     cd $cwd
}

# Function for create the PHP versions, from 7.2 to 7.4
function create_php_versions() {
    phpVersions=("7.2" "7.3" "7.4")

    echo "Creatinig php version folders"

    for phpVersion in "${phpVersions[@]}"; do
        mkdir -p "php$phpVersion"
        echo "Folder php$phpVersion created."
    done
}

# Function for copy the Config and entrypoint on each php version folder
function copy_entrypoint_config() {

    echo "Copying entrypoint, wintercms, config on each PHP/Variant folder "
    local phpVersions=( php7.*/ )
    phpVersions=( "${phpVersions[@]%/}" )

    for phpVersion in "${phpVersions[@]}"; do
        phpVersionDir="$phpVersion"
        phpVersion="${phpVersion#php}"

        for variant in apache fpm; do
            dir="$phpVersionDir/$variant"
            mkdir -p "$dir/winter"
            echo "Directory created $dir/winter"
            cp -a docker-wn-entrypoint "$dir/docker-wn-entrypoint"
            cp -r ../winter/. "$dir/winter/."
            cp -a config "$dir/."
        done
    done
}

# Update our dockerfile, only stable tags
function update_dockerfiles {

  echo "Updating Dockerfile for each PHP/Variant"
  current_tag=$VTAG
  build=${VTAG:1}
  hash=NULL

  local phpVersions=( php7.*/ )

  phpVersions=( "${phpVersions[@]%/}" )

  for phpVersion in "${phpVersions[@]}"; do
    phpVersionDir="$phpVersion"
    phpVersion="${phpVersion#php}"

    if [ "$phpVersion" == "7.4" ]; then
      gd_config="docker-php-ext-configure gd --with-jpeg --with-webp"
      zip_config="docker-php-ext-configure zip --with-zip"
    else
      gd_config="docker-php-ext-configure gd --with-png-dir --with-jpeg-dir --with-webp-dir"
      zip_config="docker-php-ext-configure zip --with-libzip"
    fi

    for variant in apache fpm; do
      dir="$phpVersionDir/$variant"
      mkdir -p "$dir"

      if [ "$variant" == "apache" ]; then
        extras="RUN a2enmod rewrite"
        cmd="apache2-foreground"
      elif [ "$variant" == "fpm" ]; then
        extras=""
        cmd="php-fpm"
      fi

      sed \
        -e '/^#.*$/d' -e '/^  #.*$/d' \
        -e 's!%%WINTERCMS_CORE_HASH%%!'"$hash"'!g' \
        -e 's!%%WINTERCMS_CORE_BUILD%%!'"$build"'!g' \
        -e 's!%%PHP_VERSION%%!'"$phpVersion"'!g' \
        -e 's!%%PHP_GD_CONFIG%%!'"$gd_config"'!g' \
        -e 's!%%PHP_ZIP_CONFIG%%!'"$zip_config"'!g' \
        -e 's!%%VARIANT%%!'"$variant"'!g' \
        -e 's!%%VARIANT_EXTRAS%%!'"$extras"'!g' \
        -e 's!%%CMD%%!'"$cmd"'!g' \
        Dockerfile.template > "$dir/Dockerfile"

    done
  done
}

## Checkout the current tag
checkout_tag

## Create PHP versions folder
create_php_versions

## Copy Repo, Entrypoints, configs into each PHP Version/variant
copy_entrypoint_config

## Update each Dockerfile per php version/variant
update_dockerfiles